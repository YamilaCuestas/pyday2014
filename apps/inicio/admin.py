from django.contrib import admin
from .models import Novedades,ColumnaUno,ColumnaDos,ColumnaCentral

admin.site.register(Novedades)
admin.site.register(ColumnaUno)
admin.site.register(ColumnaDos)
admin.site.register(ColumnaCentral)


# Register your models here.
