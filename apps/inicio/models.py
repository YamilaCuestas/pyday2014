from django.db import models

# NOVEDADES

class Novedades(models.Model):
	titulo = models.CharField(max_length=100,null=True, blank=True)
	img = models.ImageField(upload_to='img_novedades',null=True, blank=True)
	parrafo1 = models.TextField(null=True, blank=True)
	parrafo2 = models.TextField(null=True, blank=True)
	enlace = models.URLField(null=True, blank=True)

	def __unicode__(self):
		return self.titulo

class ColumnaCentral(models.Model):
	titulo = models.CharField(max_length=100,null=True, blank=True)
	img = models.ImageField(upload_to='img_columna_central',null=True, blank=True)
	parrafo1 = models.TextField(null=True, blank=True)
	parrafo2 = models.TextField(null=True, blank=True)
	enlace = models.URLField(null=True, blank=True)

	def __unicode__(self):
		return self.titulo

class ColumnaUno(models.Model):
	titulo = models.CharField(max_length=100,null=True, blank=True)
	img = models.ImageField(upload_to='img_columna_dos',null=True, blank=True)
	parrafo1 = models.TextField(null=True, blank=True)
	parrafo2 = models.TextField(null=True, blank=True)
	enlace = models.URLField(null=True, blank=True)

	def __unicode__(self):
		return self.titulo

class ColumnaDos(models.Model):
	titulo = models.CharField(max_length=100,null=True, blank=True)
	img = models.ImageField(upload_to='img_columna_uno',null=True, blank=True)
	parrafo1 = models.TextField(null=True, blank=True)
	parrafo2 = models.TextField(null=True, blank=True)
	enlace = models.URLField(null=True, blank=True)

	def __unicode__(self):
		return self.titulo
