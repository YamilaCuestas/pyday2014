from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView
from .models import Novedades,ColumnaUno,ColumnaDos,ColumnaCentral
from apps.sponsor.models import Sponsor,Categoria

def VistaInicio(request):
	sponsors = Sponsor.objects.all()
	categorias = Categoria.objects.all()
	novedades = Novedades.objects.all()
	novedades.reverse()
	ccentral = ColumnaCentral.objects.all()
	cuno = ColumnaUno.objects.all()
	cdos = ColumnaDos.objects.all()
	return render_to_response('inicio/index.html',{'sponsors':sponsors,
		'categorias':categorias,
		'novedades':novedades,
		'columnauno':cuno,
		'columnados':cdos,
		'columnacentral':ccentral,
		}
		)


def VistaJuego(request):
	return render_to_response('inicio/tetris/tetris.html',{})


