from django.db import models

class Categoria(models.Model):
	nombre = models.CharField(max_length=30)
	# Para que muestre el nombre 
	def __unicode__(self):
		return self.nombre

class Sponsor(models.Model):
	nombre = models.CharField(max_length=30)
	website = models.URLField(null=True, blank=True)
	categoria = models.ForeignKey(Categoria)
	logo = models.ImageField(upload_to='logos_sponsor')
	COLUMNAS=( ('1','Columna 1'),('2','Columna 2'),('3','Columna 3'))
	columna = models.CharField(max_length=20 ,choices=COLUMNAS,null=True, blank=True)

	def __unicode__(self):
		return self.nombre