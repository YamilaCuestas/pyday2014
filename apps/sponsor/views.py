from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.generic import TemplateView, ListView
from .models import Sponsor, Categoria

class ListarSponsor(ListView):
	#template_name = 'sponsor/base_sponsor.html'
	model = Sponsor
	# Para poder utilizarlo en el html con otro nombre:
	context_object_name='sponsors'
	#queryset = Sponsor.objects.order_by('categoria')

def VistaSponsor(request):
	sponsors = Sponsor.objects.all()
	return render_to_response('sponsor/base_sponsor.html',{'sponsors':sponsors,})
