from django.contrib import admin
from .models import Categoria, Sponsor

admin.site.register(Categoria)
admin.site.register(Sponsor)

# Register your models here.
