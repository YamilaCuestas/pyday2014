from django.db import models

class Organizadore(models.Model):
	nombre = models.CharField(max_length=100)
	V=( ('T','True'),('F','False'))
	visible = models.CharField(max_length=20 ,choices=V,null=True, blank=True)

	def __unicode__(self):
		return self.nombre

