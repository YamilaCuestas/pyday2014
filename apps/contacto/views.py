from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView
from .models import Organizadore


def Mostrar(request):
	organizadores = Organizadore.objects.all()
	return render_to_response('contacto/index.html',{'organizadores':organizadores})
