from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView
from .models import Contenido

def Mostrar(request):
	contenidos = Contenido.objects.all()
	return render_to_response('conferencia/index.html',{'contenidos':contenidos,})

def Lugar(request):
	contenidos = Contenido.objects.all()
	return render_to_response('conferencia/lugar.html',{})

def Tetris(request):
	return render_to_response('conferencia/tetris.html',{})
