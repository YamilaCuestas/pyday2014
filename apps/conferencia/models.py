from django.db import models

class Contenido(models.Model):
	titulo = models.CharField(max_length=100,null=True, blank=True)
	img = models.ImageField(upload_to='img_contenido',null=True, blank=True)
	parrafo1 = models.TextField(null=True, blank=True)
	parrafo2 = models.TextField(null=True, blank=True)
	enlace = models.URLField(null=True, blank=True)
	V=( ('T','True'),('F','False'))
	visible = models.CharField(max_length=20 ,choices=V,null=True, blank=True)

	def __unicode__(self):
		return self.titulo
