from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView
from .models import Contenido


def Mostrar(request):
	contenidos = Contenido.objects.all()
	return render_to_response('inscripcion/index.html',{'contenidos':contenidos,})

def Formulario(request):
	return render_to_response('inscripcion/formulario.html',{})
