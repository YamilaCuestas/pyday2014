# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from .views import *
from django.conf import settings

urlpatterns = patterns('',
	url(r'^$','apps.inscripcion.views.Mostrar'),
	url(r'^formulario/','apps.inscripcion.views.Formulario'),
	url(r'^media/(?P<path>.*$)', 'django.views.static.serve',
		{'document_root': settings.MEDIA_ROOT,}),
)