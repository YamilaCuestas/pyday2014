# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from .views import *
from django.conf import settings

urlpatterns = patterns('',
	url(r'^$','apps.charlas.views.Mostrar'),
	url(r'^cfc/','apps.charlas.views.CFC'),
	url(r'^cfc_full','apps.charlas.views.CFCF'),
	url(r'^media/(?P<path>.*$)', 'django.views.static.serve',
		{'document_root': settings.MEDIA_ROOT,}),
)
