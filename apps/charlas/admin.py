from django.contrib import admin
from .models import Charla, Disertante, Contenido

admin.site.register(Contenido)
admin.site.register(Disertante)
admin.site.register(Charla)
