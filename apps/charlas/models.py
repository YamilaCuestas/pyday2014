from django.db import models

class Contenido(models.Model):
	titulo = models.CharField(max_length=100,null=True, blank=True)
	parrafo1 = models.TextField(null=True, blank=True)
	parrafo2 = models.TextField(null=True, blank=True)
	parrafo3 = models.TextField(null=True, blank=True)
	enlace = models.URLField(null=True, blank=True)
	V=( ('T','True'),('F','False'))
	visible = models.CharField(max_length=20 ,choices=V,null=True, blank=True)

	def __unicode__(self):
		return self.titulo

class Disertante(models.Model):
	nombre = models.CharField(max_length=100)
	correo = models.CharField(max_length=100,null=True, blank=True)
	telefono = models.CharField(max_length=20,null=True, blank=True)
	descripcion = models.TextField(null=True, blank=True)
	V=( ('T','True'),('F','False'))
	visible = models.CharField(max_length=20 ,choices=V,null=True, blank=True)

	def __unicode__(self):
		return self.nombre

class Charla(models.Model):
	titulo = models.CharField(max_length=100,null=True, blank=True)
	disertante = models.ForeignKey(Disertante)
	N=( ('Principiante','Principiante'),('Intermedio','Intermedio'),('Avanzado','Avanzado'))
	nivel = models.CharField(max_length=20 ,choices=N,null=True, blank=True)
	T=( ('Charla','Charla'),('Taller','Taller'),('Debate','Debate'))
	tipo = models.CharField(max_length=20 ,choices=T,null=True, blank=True)
	descripcion = models.TextField(null=True, blank=True)	
	duracion = models.CharField(max_length=10,null=True, blank=True)
	hora_inicio = models.CharField(max_length=10,null=True, blank=True)
	hora_finalizacion = models.CharField(max_length=10,null=True, blank=True)
	aula = models.CharField(max_length=10,null=True, blank=True)
	enlace = models.URLField(null=True, blank=True)
	E=(('Aceptada','Aceptada'),('Rechazada','Rechazada'),('Pendiente','Pendiente'))
	estado = models.CharField(max_length=20 ,choices=E,null=True, blank=True)

	def __unicode__(self):
		return self.titulo