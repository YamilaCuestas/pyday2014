from django.shortcuts import render_to_response
from django.views.generic import TemplateView,ListView
from .models import Disertante, Charla, Contenido


def Mostrar(request):
	contenidos = Contenido.objects.all()
	charlas = Charla.objects.all()
	disertantes = Disertante.objects.all()
	return render_to_response('charlas/index.html',{'contenidos':contenidos,'charlas':charlas,'disertantes':disertantes})

def CFC(request):
	return render_to_response('charlas/cfc.html',{})

def CFCF(request):
	return render_to_response('charlas/cfc_full.html',{})