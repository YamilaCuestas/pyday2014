Pyday 2014
==========

Repositorio del proyecto Django usado para la administracion y publicacion de noticias del Pyday Luján 2014.

Proceso de actualizacion del server
-----------------------------------

En el server seguramente hay cambios que no se pueden stagear o commitear, por eso hay que ocultarlos y mergear cambios.

```
#!bash
# Ir al directorio raiz
cd ~/webapps/pyday2014/pyday2014/
# Ocultar personalizaciones del servidor
git stash
# Mergear cambios (git status debe dar vacio)
git pull
# Restaurar cambios locales
git stash apply
# Si todo se mergeo sin problemas, y no se desea conservar el stash, se borra
git stash clear
# Deployar archivos estaticos
python2.7 manage.py collectstatic
```
