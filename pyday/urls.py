from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pyday.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #imagenes de sponsor
	url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
		{'document_root': settings.MEDIA_ROOT,}),
    # Administrador
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$' , include('apps.inicio.urls')),
    url(r'^inscripcion/' , include('apps.inscripcion.urls')),
    url(r'^charlas/' , include('apps.charlas.urls')),
    url(r'^conferencia/' , include('apps.conferencia.urls')),
    url(r'^contacto/' , include('apps.contacto.urls')),
)
